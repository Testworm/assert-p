#!/usr/bin/env python3
# _*_ coding: utf-8 _*_
# @Author : quality assurance @软件质量保障
# @Email: byteflow@163.com
# @desc : 本工具支持两个字典一致性compare，可用于自动化断言，例如数据库实际落库数据与预期结果比较；
# 支持场景：1.落单条数据场景 2.落多条数据场景
# 使用本工具前注意事项：
# 1.安装依赖库，texttable，参见requirements.txt
# 2.需要将断言的字典值转化为string格式, 否则会报类型错误：
# 例如TypeError: argument of type 'int' is not iterable


from unittest import TestCase
from AssertP import AssertPy, AssertPy_multi


class Test(TestCase):
    # same：测试通过
    def test_assert_py_pass(self):
        tName = "TestTable"
        act = {'user_id': '20210100002', 'pay_status': 'SUCCESS'}
        exp = {'user_id': '20210100002', 'pay_status': 'SUCCESS'}
        self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # ext不同,failed
    def test_assert_py_ext_not_same(self):
        tName = "TestTable"
        act = {'pay_amount': "2000", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "USD"}'}
        exp = {'pay_amount': "2000", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), False, "assert failed")

    # NOT NONE
    def test_assert_py_not_none(self):
        tName = "TestTable"
        act = {'pay_amount': "2000", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'pay_amount': "NOT NONE", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # "", failed
    def test_assert_py_empty(self):
        tName = "TestTable"
        act = {'pay_amount': "", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'pay_amount': "NOT NONE", 'pay_cur': 'RMB',
               'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), False, "assert failed")

    # CONTAINS_CHECK: true
    def test_assert_py_contains_check_pass(self):
        tName = "TestTable"
        act = {'pay_amount': "2000", 'pay_cur': 'RMB',
           'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'pay_amount': "CONTAINS_CHECK:2000", 'pay_cur': 'RMB',
           'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # CONTAINS_CHECK: failed
    def test_assert_py_contains_check_not_pass(self):
        tName = "TestTable"
        act = {'pay_amount': "2001", 'pay_cur': 'RMB',
           'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'pay_amount': "CONTAINS_CHECK:2000", 'pay_cur': 'RMB',
           'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), False, "assert failed")

    # ext字典长度不一致
    def test_assert_py_dict_len(self):
        tName = "TestTable"
        act = {'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'ext': 'JSON:{"user_name": "QA", "currency": "RMB", "test": "db"}'}

        self.assertEqual(AssertPy(tName, act, exp), False, "assert failed")

    # JSON 不同
    def test_assert_py_json_not_same(self):
        tName = "TestTable"
        act = {'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'ext': 'JSON:{"user_name": "QA", "currency": "USD"}'}
        self.assertEqual(AssertPy(tName, act, exp), False, "assert failed")

    # JSON same,pass
    def test_assert_py_json_same(self):
        tName = "TestTable"
        act = {'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'ext': 'JSON:{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # CONTAINS_CHECK: + JSON 联合
    def test_assert_py_json_and_contains(self):
        tName = "TestTable"
        act = {'pay_amount': "2000", 'pay_cur': 'RMB',
           'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
        exp = {'pay_amount': "CONTAINS_CHECK:2000", 'pay_cur': "RMB",
           'order_id': '2022010818343700000001', 'ext': 'JSON:{"user_name": "QA", "currency": "RMB"}'}
        self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # int数据，暂不支持int类型数据check
    # def test_assert_py_json_and_contains(self):
    #     tName = "TestTable"
    #     act = {'user_id': '20210100001', 'pay_status': 'SUCCESS', 'pay_amount': 2000, 'pay_cur': 'RMB',
    #            'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
    #     exp = {'user_id': '20210100001', 'pay_status': 'SUCCESS', 'pay_amount': 2000, 'pay_cur': 'RMB',
    #            'order_id': '2022010818343700000001', 'ext': '{"user_name": "QA", "currency": "RMB"}'}
    #     self.assertEqual(AssertPy(tName, act, exp), True, "assert failed")

    # multi check, failed
    def test_assert_py_json_and_contains(self):
        tName = "TestTable"
        act = [{'order_id': '2022010818343700000003', 'user_id': '20210100002', 'pay_status': 'PAYINGi'},
               {'order_id': '2022010818343700000004', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        exp = [{'order_id': '2022010818343700000003', 'user_id': '20210100002', 'pay_status': 'PAYING'},
               {'order_id': '2022010818343700000004', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        self.assertEqual(AssertPy_multi(tName, act, exp), False, "assert failed")

    # multi check, same
    def test_assert_py_multi_same(self):
        tName = "TestTable"
        act = [{'order_id': '2022010818343700000003', 'user_id': '20210100002', 'pay_status': 'PAYING'},
               {'order_id': '2022010818343700000003', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        exp = [{'order_id': '2022010818343700000003', 'user_id': '20210100002', 'pay_status': 'PAYING'},
               {'order_id': '2022010818343700000003', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        self.assertEqual(AssertPy_multi(tName, act, exp), True, "assert failed")

    # multi check, len不同,
    def test_assert_py_multi_len(self):
        tName = "TestTable"
        act = [{'order_id': '2022010818343700000003', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        exp = [{'order_id': '2022010818343700000003', 'user_id': '20210100002', 'pay_status': 'PAYING'},
               {'order_id': '2022010818343700000003', 'user_id': '20210100001', 'pay_status': 'SUCCESS'}]
        self.assertEqual(AssertPy_multi(tName, act, exp), False, "assert failed")


if __name__ == '__main__':
    Test.main()